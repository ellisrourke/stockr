import React, { Component, Fragment} from 'react';
import '../App.css';

class Stock extends Component {
    state = {  }

    render() { 
        return (
            <Fragment>
                <div className="stockBox">
                    <h2>{this.props.name}</h2>
                    <h3>Symbol: {this.props.symbol}</h3>
                    <h3>price: ${this.props.price}</h3>
                </div>
            </Fragment>
        );
    }
}
 
export default Stock;