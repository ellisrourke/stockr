import './App.css';
import Stock from './components/stock.js'
import {useState, useEffect} from 'react';
import firebase from 'firebase/app';
import 'firebase/firestore'
import 'firebase/auth';

import { useAuthState } from 'react-firebase-hooks/auth'
//import { useCollectionData } from 'react-firebase-hooks/firestore'

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyClWbgitkvMjw4UX3vBDgs5Op85TMjY5Aw",
    authDomain: "devchat-9a9d8.firebaseapp.com",
    projectId: "stockr-dc23e",
    storageBucket: "devchat-9a9d8.appspot.com",
    messagingSenderId: "224409381743",
    appId: "1:224409381743:web:a0460d0570d21468d5169a",
    measurementId: "G-PMHXQS84NN"
  })
}else {
  firebase.app(); // if already initialized, use that one
}

const auth = firebase.auth();
const firestore = firebase.firestore();

function App() {
  const [user] = useAuthState(auth);
  const [stocks, setStocks] = useState([])

  const fetchStocks=async()=>{
    const response=firestore.collection('stocks').get();
    const data = await response.get();
    data.docs.forEach(item=>{
      setStocks([...stocks, item.data()])
    })

  }

  useEffect(() => {
    fetchStocks();
    console.log(stocks)
  }, []);
    

// {user ?  : <SignIn />}
  return (
    <div className="App">
      <script src="/__/firebase/8.2.6/firebase-app.js"></script>
      <script src="/__/firebase/8.2.6/firebase-analytics.js"></script>
      <script src="/__/firebase/init.js"></script>
      <body>

      <div class="topnav">
        <a class="active" href="#home">Home</a>
        <a href="#news">News</a>
        <a href="#contact">Contact</a>
        <a href="#about">About</a>
      </div>

      <div>

     

        {stocks.map(stock =>{
          return <Stock key={stock.uid} name={stock.name} symbol={stock.symbol} price={stock.price}/>
        })
        }
      </div>





      </body>




    </div>
  );

  function SignIn(){
    const signInWithGoogle = () => {
      const provider = new firebase.auth.GoogleAuthProvider();
      auth.signInWithPopup(provider);
    }
    return ( <button onClick={signInWithGoogle}>Sign in with Google</button> )
  }

  function SignOut(){
    return auth.currentUser && ( <button onClick={() => auth.signOut }>Sign Out</button>)
  }




}

export default App;
